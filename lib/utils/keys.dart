class KeysMovie {
  // API
  static const String APPLICATION_JSON = 'application/json';
  static const String TOKEN = 'cache_token_id';
  static const String BASE_URL = 'https://api.themoviedb.org/3/movie/';
  static const String IMAGE_URL = 'https://image.tmdb.org/t/p/w500/';
}
