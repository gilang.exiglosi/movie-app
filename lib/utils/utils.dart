import 'dart:convert';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class Utils {
  // method yg bakal sering dipakai

  exampleMethod(msg) {
    print("message"); // printing message
  }

  static void myPushPage(Widget pagePush, context) {
    Navigator.push(context, MaterialPageRoute(builder: (contexts) {
      print('### myPushPage() -> $pagePush');
      return pagePush;
    }));
  }

  static void myPushPageReplacement(Widget pagePush, context) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (contexts) {
      print('### myPushPageReplacement() -> $pagePush');
      return pagePush;
    }));
  }

  static void myPushAndRemoveUntil(Widget pagePush, context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => pagePush),
        (Route<dynamic> route) => false);
  }

  static String randomTrxID() {
    var floors = (math.Random.secure().nextDouble() * 100000000090).floor();
    var floorData = "1009" + floors.toString();
    var trxID = "1009" + floorData;
    if (trxID.length > 10) {
      trxID = trxID + floors.toString();
    }
    if (trxID.length > 15) {
      trxID = trxID.substring(0, 15);
    }
    print('### randomTrxID() -> $trxID');
    return trxID;
  }
}
