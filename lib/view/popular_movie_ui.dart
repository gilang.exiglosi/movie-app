import 'package:flutter/material.dart';
import 'package:movie_app/model/popular_movie.dart';
import 'package:movie_app/utils/utils.dart';
import 'package:movie_app/view/detail_movie.dart';
import 'package:movie_app/widget/card_movie.dart';

class PopularMovieUi extends StatelessWidget {
  final PopularMovie popularMovie;
  final String title;
  const PopularMovieUi({Key key, this.popularMovie, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Row(
          children: [
            Expanded(
              flex: 3,
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            Expanded(child: GestureDetector(child: Text("Show More")))
          ],
        ),
        subtitle: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: popularMovie.results.length > 3
                ? 3
                : popularMovie.results.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  Utils.myPushPage(
                      DetailMovie(
                        idMovie: popularMovie.results[index].id,
                      ),
                      context);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CardMovie(
                    thumbnail: popularMovie.results[index].posterPath,
                  ),
                ),
              );
            }),
      ),
    );
  }
}
