import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/bloc/home/home_bloc.dart';
import 'package:movie_app/view/popular_movie_ui.dart';

class HomeUi extends StatefulWidget {
  HomeUi({Key key}) : super(key: key);

  @override
  _HomeUiState createState() => _HomeUiState();
}

class _HomeUiState extends State<HomeUi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => HomeBloc()..add(GetHome()),
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeInitial) {
              return Center(
                child: Container(
                  height: 50,
                  width: 50,
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is OnHome) {
              return SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: [
                      PopularMovieUi(
                        title: "Now Playing",
                        popularMovie: state.nowPlaying,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      PopularMovieUi(
                        title: "Popular",
                        popularMovie: state.popularMovie,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      PopularMovieUi(
                        title: "UpComing",
                        popularMovie: state.upComing,
                      ),
                    ],
                  ),
                ),
              );
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
