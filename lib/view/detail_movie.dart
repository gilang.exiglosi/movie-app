import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/bloc/home/home_bloc.dart';
import 'package:movie_app/widget/detail_movie_widget.dart';

class DetailMovie extends StatelessWidget {
  final int idMovie;
  const DetailMovie({Key key, this.idMovie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => HomeBloc()..add(GetDetailMovie(idMovie)),
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is OnMovieDetail) {
              return Scaffold(
                body: DetailMovieWidget(
                  detailMovieModel: state.detailMovieModel,
                  reviewMovie: state.reviewMovie,
                ),
              );
            }
            return Center(
              child: Container(
                child: CircularProgressIndicator(),
              ),
            );
          },
        ),
      ),
    );
  }
}
