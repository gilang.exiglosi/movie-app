import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movie_app/model/detail_movie_model.dart';
import 'package:movie_app/model/review_movie.dart';
import 'package:movie_app/utils/keys.dart';

class DetailMovieWidget extends StatelessWidget {
  final DetailMovieModel detailMovieModel;
  final ReviewMovie reviewMovie;
  const DetailMovieWidget({Key key, this.detailMovieModel, this.reviewMovie})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Stack(
          children: [
            Container(
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        alignment: Alignment.topCenter,
                        width: MediaQuery.of(context).size.width,
                        child: Image.network(KeysMovie.IMAGE_URL +
                            detailMovieModel.backdropPath)),
                    Container(
                      padding: EdgeInsets.only(left: 130, top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            detailMovieModel.originalTitle,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          Container(
                            child: RatingBar.builder(
                              initialRating: detailMovieModel.voteAverage / 2,
                              minRating: 1,
                              direction: Axis.horizontal,
                              itemSize: 20,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                          ),
                          Text(
                            detailMovieModel.releaseDate
                                .toString()
                                .substring(0, 4),
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      child: ListTile(
                        title: Text("Genre"),
                        subtitle: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: detailMovieModel.genres
                                .map((e) => Text(
                                      e.name,
                                      textAlign: TextAlign.justify,
                                    ))
                                .toList()),
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      child: ListTile(
                        title: Text("Overview"),
                        subtitle: Text(
                          detailMovieModel.overview,
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                    Container(
                      child: ListTile(
                          title: Text("Review"),
                          subtitle: ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: reviewMovie.results.length,
                              itemBuilder: (context, index) {
                                return Text(
                                  reviewMovie.results[index].content,
                                  textAlign: TextAlign.justify,
                                );
                              })),
                    ),
                  ],
                )),
            Container(
                margin: EdgeInsets.only(top: 150, left: 10),
                width: MediaQuery.of(context).size.width / 5,
                height: MediaQuery.of(context).size.height / 5,
                child: Image.network(
                    KeysMovie.IMAGE_URL + detailMovieModel.posterPath)),
          ],
        ),
      ),
    );
  }
}

//  Positioned(
//                 bottom: 450,
//                 child:
//                 )),
