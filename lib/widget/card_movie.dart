import 'package:flutter/material.dart';
import 'package:movie_app/utils/keys.dart';

class CardMovie extends StatelessWidget {
  final thumbnail;
  const CardMovie({Key key, this.thumbnail}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(12),
      //   color: Colors.white,
      //   boxShadow: [
      //     BoxShadow(
      //       color: Color(0xff00000017).withOpacity(0.02),
      //       blurRadius: 19,
      //       offset: Offset(0, 8),
      //     ),
      //   ],
      // ),
      child: Container(child: Image.network(KeysMovie.IMAGE_URL + thumbnail)),
    );
  }
}
