import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app/model/detail_movie_model.dart';
import 'package:movie_app/model/popular_movie.dart';
import 'package:movie_app/model/review_movie.dart';
import 'package:movie_app/service/http_request.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is GetHome) {
      yield* mapEventGetHomeToState();
    } else if (event is GetDetailMovie) {
      yield* mapEventGetDetailToState(event.idMovie);
    }
  }

  Stream<HomeState> mapEventGetHomeToState() async* {
    yield HomeInitial();
    final url =
        "popular?api_key=d79e37033d3b0597a155fd7933ce3c9b&language=en-US&page=1";
    final data = await Network.getRequest(url);
    final urlUpComing =
        "upcoming?api_key=d79e37033d3b0597a155fd7933ce3c9b&language=en-US&page=1";
    final dataUpComing = await Network.getRequest(urlUpComing);
    final urlNowPlaying =
        "now_playing?api_key=d79e37033d3b0597a155fd7933ce3c9b&language=en-US&page=1";
    final dataNowPlaying = await Network.getRequest(urlNowPlaying);
    PopularMovie nowPlaying =
        PopularMovie.fromJson(json.decode(dataNowPlaying));
    PopularMovie popularMovie = PopularMovie.fromJson(json.decode(data));
    PopularMovie upComing = PopularMovie.fromJson(json.decode(dataUpComing));
    yield OnHome(popularMovie, nowPlaying, upComing);
  }

  Stream<HomeState> mapEventGetDetailToState(idMovie) async* {
    yield HomeInitial();
    final url =
        "$idMovie?api_key=d79e37033d3b0597a155fd7933ce3c9b&language=en-US&page=1";
    final urlReview =
        "$idMovie/reviews?api_key=d79e37033d3b0597a155fd7933ce3c9b&language=en-US&page=1";
    final data = await Network.getRequest(url);
    final dataReview = await Network.getRequest(urlReview);
    ReviewMovie reviewMovie = ReviewMovie.fromJson(json.decode(dataReview));
    DetailMovieModel detailMovieModel =
        DetailMovieModel.fromJson(json.decode(data));
    yield OnMovieDetail(detailMovieModel, reviewMovie);
  }
}
