part of 'home_bloc.dart';

class HomeEvent {}

class GetHome extends HomeEvent {}

class GetDetailMovie extends HomeEvent {
  final int idMovie;

  GetDetailMovie(this.idMovie);
}
