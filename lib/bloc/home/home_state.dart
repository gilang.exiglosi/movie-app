part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class OnHome extends HomeState {
  final PopularMovie popularMovie;
  final PopularMovie nowPlaying;
  final PopularMovie upComing;

  OnHome(this.popularMovie, this.nowPlaying, this.upComing);
}

class OnMovieDetail extends HomeState {
  final DetailMovieModel detailMovieModel;
  final ReviewMovie reviewMovie;

  OnMovieDetail(this.detailMovieModel, this.reviewMovie);
}
