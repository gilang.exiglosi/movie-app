import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:movie_app/utils/keys.dart';

class Network {
  static Future<String> postRequest(String url,
      {Map<String, dynamic> body}) async {
    String result = "";
    print("### Request: $body");
    print("### Base Url: ${KeysMovie.BASE_URL + url}");

    try {
      final response = await http
          .post(
            KeysMovie.BASE_URL + url,
            headers: {HttpHeaders.contentTypeHeader: 'application/json'},
            body: json.encode(body),
          )
          .timeout(Duration(minutes: 3));
      print("### Response: ${response.body}");
    } catch (e) {
      print('### Error: ${e.message}');
    }
    return result;
  }

  static Future<String> getRequest(String url) async {
    String result = "";

    print("### Base Url: ${KeysMovie.BASE_URL + url}");

    try {
      final response = await http.get(
        KeysMovie.BASE_URL + url,
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      ).timeout(Duration(minutes: 3));
      print("### Response: ${response.body}");
      result = response.body.toString();
    } catch (e) {
      print('### Error: ${e.message}');
    }
    return result;
  }
}
